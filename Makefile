
%.byte: %.ml
	ocamlfind ocamlc -package qcheck -package crowbar -package alcotest  -package qcheck-alcotest -package js_of_ocaml -package js_of_ocaml-ppx -linkpkg -o $(<:.ml=.byte) $<

%.js: %.byte
	js_of_ocaml $<

# qcheck_jsoo.js: qcheck_jsoo.ml
# crowbar_jsoo.js: qcheck_jsoo.ml

all: crowbar_jsoo.js qcheck_simple_jsoo.js qcheck_runtests_jsoo.js qcheck_alcotest_jsoo.js


clean:
	rm -f *.byte *.js *.cmi *.cmo
