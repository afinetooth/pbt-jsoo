
let () =
  let open Crowbar in
  add_test
    ~name:"add comm"
    [int; int]
    (fun a b -> Crowbar.check_eq (a + b) (b + a))
