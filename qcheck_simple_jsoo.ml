let test_add_comm =
  QCheck.Test.make ~name:"add comm" ~count:1000
    (QCheck.pair QCheck.int QCheck.int)
    (fun (a,b) -> Arith.add a b = Arith.add b a)

let () = QCheck.Test.check_exn test_add_comm

